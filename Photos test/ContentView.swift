//
//  ContentView.swift
//  Photos test
//
//  Created by david florczak on 07/12/2019.
//  Copyright © 2019 david florczak. All rights reserved.
//

import UIKit
import SwiftUI
import Pages
import Photos


var images = [UIImage]([#imageLiteral(resourceName: "token_1")])

var photoAsset = PHAsset.fetchAssets(with: .image, options: nil) as! PHFetchResult<AnyObject>
var assetCollection = PHAssetCollection()
let targetSize: CGSize = UIScreen.main.bounds.size
let contentMode: PHImageContentMode = .aspectFill

func loadImages() {
    
    photoAsset.enumerateObjects {
        object, index, stop in

        let options = PHImageRequestOptions()
        options.isSynchronous = true
        options.deliveryMode = .highQualityFormat

        PHImageManager.default().requestImage(for: (object as! PHAsset) as PHAsset, targetSize: targetSize, contentMode: contentMode, options: options) {
            image, info in
            images.append(image!)
        }
    }
}



struct ContentView: View {
    
    @State var index: Int = 0
    
    var body: some View {
        
        VStack(alignment: .center) {
            
            ModelPages(images, currentPage: $index, hasControl: false) { index, image in
                Image(uiImage: image)
                    .resizable()
                    .scaledToFit()
                    .frame(minWidth: 0, maxWidth: UIScreen.main.bounds.width-20, minHeight: 0, maxHeight: UIScreen.main.bounds.height)
                    .clipped()
            }
            
            ScrollView(.horizontal) {
                HStack(alignment: .center) {
                    ForEach(0..<images.count) { img in
                        Image(uiImage: images[img])
                            .resizable()
                            .scaledToFill()
                            .frame(minWidth: 0, maxWidth: UIScreen.main.bounds.height/20, minHeight: 0, maxHeight: UIScreen.main.bounds.height/20)
                            .clipped()
                    }
                }
                }.frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height/15)
        }.onAppear() {
            
        }
    }
}

var imageIndex = 0

struct ImageView: View {
    
    @State private var currentPosition: CGPoint = .zero
    @State private var newPosition: CGPoint = .zero
    
    var comparePosition: Int = 0
    
    var body: some View {
        Image(uiImage: images[imageIndex])
            .offset(x: self.currentPosition.x, y: self.currentPosition.y)
            .gesture(DragGesture()
                .onChanged { value in
                    self.currentPosition = CGPoint(x: value.translation.width + self.newPosition.x, y: 0)
                    
                    
                    
                    if self.currentPosition.x - self.newPosition.x > 50 {
                        imageIndex += 1
                        if imageIndex > images.count-1 { imageIndex = 0 }
                        
                    } else if self.currentPosition.x - self.newPosition.x < -50 {
                        imageIndex -= 1
                        if imageIndex < 0 { imageIndex = images.count-1 }
                    }
            }   // 4.
                .onEnded { value in
                    self.currentPosition = CGPoint(x: value.translation.width + self.newPosition.x, y: 0)
                    self.newPosition = self.currentPosition
                }
        )
        
    }
    
    func changePic() -> Bool {
        return self.currentPosition.x - self.newPosition.x > 50
    }
    
}

struct MainPic: View {
    
    var body: some View {
        List() {
            Image("token_1")
            Image("token_2")
            Image("token_3")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            ContentView()
            //            MainPic()
//            ImageView()
            //            ImageView()
        }
    }
}
